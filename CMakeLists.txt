cmake_minimum_required(VERSION 3.0.2)
project(rospy_nodeutils)

find_package(catkin REQUIRED COMPONENTS
  #cmake_modules
  #roscpp
  rospy
)

#find_package(NUMPY REQUIRED)

catkin_python_setup()

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES rospy_nodeutils
  CATKIN_DEPENDS rospy
#  DEPENDS system_lib
)
