import functools
import rospy
from std_msgs.msg import String

def deco1(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        print('wrap')
        value = func(*args, **kwargs)
        return value
    return wrapper

def advertise(topic):
    def inner_advertise(func):
        print('advertising to %s'%topic)
        return func
    return inner_advertise


class RosWrapper(object):
    def __init__(self):
        self.subs = []
        self.pubs = {}

    def subscribe(self, topic, msgtype):
        def subscriber(cb):
            self.subs.append(rospy.Subscriber(topic, msgtype, cb))
        return subscriber



ros_wrapper = RosWrapper()

@ros_wrapper.subscribe('/foo', String)
def foo_img_cb(self, msg):
    print msg

node = Node()

rospy.init_node('bla')
rospy.spin()
