
from StringIO import StringIO

import numpy as np
import traitlets as tr
import cv2
import cv_bridge

import rospy
import rospkg

from spath import Path

from .health_monitor import HealthMonitor


class BaseNode(tr.HasTraits):
    def __init__(self, node_name):
        super(BaseNode, self).__init__()
        self.node_name = node_name
        self.publishers = {}
        self.subscribers = []
        self.monitor = HealthMonitor(node_name, self)

        rospy.init_node(self.node_name)
        rospy.on_shutdown(self._shutdown)

    def init_traits_rosparam(self):
        for name in self.trait_names():
            default_param = getattr(self, name)
            ros_param = rospy.get_param('~{}'.format(name), None)
            if ros_param is None:
                rospy.loginfo('param name: %s, using default: %s',
                              name,
                              default_param)
                param = default_param
            else:
                rospy.loginfo('param name: %s, using rosparam: %s',
                              name,
                              ros_param)
                param = ros_param
            setattr(self, name, param)

    def get_pkg_path(self, pkg_name):
        rospack = rospkg.RosPack()
        return Path(rospack.get_path(pkg_name))

    def tic(self, id_):
        self.monitor.tic(id_)

    def toc(self, id_):
        return self.monitor.toc(id_)

    def get_private_param(self, name, default=None):
        return rospy.get_param('~{}'.format(name), default)

    def get_param(self, name, default=None):
        return rospy.get_param(name, default)

    def advertise(self, topic, msgtype, queue_size=1):
        self.publishers[topic] = rospy.Publisher(topic, msgtype, queue_size=queue_size)

    def publish(self, topic, msg):
        self.publishers[topic].publish(msg)

    def has_subscribers(self, topic):
        return (self.publishers[topic].get_num_connections() > 0)

    def subscribe(self, topic, msgtype, cb):
        sub = rospy.Subscriber(topic, msgtype, cb)
        self.subscribers.append(sub)

    def publish_image(self,
                      topic,
                      img,
                      encoding='passthrough',
                      stamp=None,
                      frame_id=None):
        bridge = cv_bridge.CvBridge()
        imgmsg = bridge.cv2_to_imgmsg(img, encoding=encoding)
        imgmsg.encoding = encoding
        if stamp is not None:
            imgmsg.header.stamp = stamp
        if frame_id is not None:
            imgmsg.header.frame_id = frame_id
        self.publishers[topic].publish(imgmsg)

    def load_msg(self, msgtype, fname):
        msg = msgtype()
        with open(fname, 'r') as f:
            msg.deserialize(f.read())
        return msg

    def save_msg(self, fname):
        buf = StringIO()
        buf.serialize(buf)
        buf.seek(0)
        with open(fname, 'w') as f:
            f.write(buf.read())

    def _shutdown(self):
        self.monitor.print_time_statistics()
        self.shutdown()

    def shutdown(self):
        """ this may be overridden """
        pass

    def run(self):
        rospy.spin()

    # TODO consider a decorator subscribe image
    # TODO consider ros decorator, see roshelper
